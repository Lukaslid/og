﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GameServer.Migrations
{
    public partial class editedFighter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "upgradePrice",
                table: "Fighters",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "upgradePrice",
                table: "Fighters");
        }
    }
}
