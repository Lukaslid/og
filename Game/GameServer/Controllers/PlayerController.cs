﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GameServer.Models;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameServer.Controllers
{
    [Route("api/player")]
    [ApiController]
    public class PlayerController : ControllerBase
    {

        static HttpClient client = new HttpClient();
        static string mediaType = "application/json";
        private readonly HavanaContext _context;
        public int Qty { get; set; } = 0;

        public PlayerController(HavanaContext context)
        {
            _context = context;
        }

        // GET api/player
        [HttpGet]
        public ActionResult<IEnumerable<Player>> GetAll()
        {
            return _context.Players.ToList();
        }

        // GET api/player/5
        [HttpGet("{ID}", Name = "GetPlayer")]
        public ActionResult<Player> GetById(int id)
        {
            Player p = _context.Players.Find(id);
            if (p == null)
            {
                return NotFound("player not found");
            }
            return p;
        }

        // POST api/player
        [HttpPost]
        //public string Create(Player player)
        public ActionResult<Player> Create([FromBody] Player player)
        {
            _context.Players.Add(player);
            _context.SaveChanges();

            //return Ok(); //"created - ok"; 
            return CreatedAtRoute("GetPlayer", new { id = player.ID }, player); 
        }


        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Player p)
        {
            var pp = _context.Players.Find(id);
            if (pp == null)
            {
                return NotFound();
            }

            pp.username = p.username;
            pp.gold = p.gold;

            _context.Players.Update(pp);
            _context.SaveChanges();

            return Ok(); //NoContent();
        }

        //[HttpPatch]
        //public IActionResult PartialUpdate([FromBody] Coordinates request)
        //{
        //    var player = _context.Players.Find(request.Id);
        //    if (player == null)
        //    {
        //        return NotFound();
        //    }
        //    else
        //    {
        //        player.PosX = request.PosX;
        //        player.PosY = request.PosY;

        //        _context.Players.Update(player);
        //        _context.SaveChanges();
        //    }
        //    return Ok();
        //    //return CreatedAtRoute("GetPlayer", new { id = player.Id }, player);
        //}



        // DELETE api/values/5
        /*[HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var todo = _context.Players.Find(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Players.Remove(todo);
            _context.SaveChanges();
            return NoContent();
        }


        // GET api/player/5/ping
        [HttpGet("{ID}", Name = "GetPlayerGameStatus")]
        public HttpResponseMessage GetStatusById(int id)
        {
            Player p = _context.Players.Find(id);
            if (p == null)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            Game g = p.game;
            if (g.players.Count == 1)
                return new HttpResponseMessage(HttpStatusCode.Found);
            if (g.players.Count == 2)
                return new HttpResponseMessage(HttpStatusCode.OK);
            return new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }


        // GET api/player/5/game
        [HttpGet("{ID}", Name = "CreateGame")]
        public HttpResponseMessage CreateGame(int id)
        {
            Player p = _context.Players.Find(id);
            if (p == null || p.game != null)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            _context.Games.ToList();
            return new HttpResponseMessage(HttpStatusCode.Accepted);


        }
    }
}




