﻿using System;
using System.Collections.Generic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GameServer.Models;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameServer.Controllers
{
    [Route("api/fighter")]
    [ApiController]
    public class FighterController : ControllerBase
    {
        private readonly HavanaContext _context;
        public int Qty { get; set; } = 0;
        public double costsByLevelCoef = 2.5; // spawninimo ir upgradinimo kainos koficientas 


        public FighterController(HavanaContext context)
        {
            _context = context;

            //if (_context.Fighters.Count() < 5)
            //{
            //    for (int i = 0; i < 3; i++)
            //    {
            //        Qty++;
            //        Player player = new Player("playeriss", 0, 250);
            //        _context.Players.Add(player);
            //        Fighter f = new Fighter(Qty, 1, 1, 1, 1, 1, 1, 1, "blue", 1, player);
            //        _context.Fighters.Add(f);
            //    }        
            //}
            //_context.SaveChanges();
        }


        // GET api/fighter
        [HttpGet]
        public ActionResult<IEnumerable<Fighter>> GetAll()
        {
            return _context.Fighters.ToList();
        }

        // GET api/fighter/5
        [HttpGet("{ID}", Name = "GetFighter")]
        public ActionResult<Fighter> GetById(int id)
        {
            Fighter p = _context.Fighters.Find(id);
            if (p == null)
            {
                return NotFound("Fighter not found");
            }
            return p;
        }

        // POST api/fighter
        [HttpPost]
        public ActionResult<Fighter> Create([FromBody] Fighter fighter)
        {
            _context.Fighters.Add(fighter);
            _context.SaveChanges();

            //return Ok(); //"created - ok"; 
            return CreatedAtRoute("GetPlayer", new { id = fighter.ID }, fighter);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Fighter f)
        {
            var ff = _context.Fighters.Find(id);
            if (ff == null)
            {
                return NotFound();
            }

            ff.hitpoints = f.hitpoints;
            ff.current_health = f.current_health;
            ff.damage = f.damage;
            ff.movement_speed = f.movement_speed;
            ff.cooldown = f.cooldown;
            ff.attack_speed = f.attack_speed;
            ff.attack_range = f.attack_range;
            ff.price = f.price;
            ff.color = f.color;
            ff.level = f.level;
            ff.position = f.position;
            ff.price = f.price;
            ff.upgradePrice = f.upgradePrice;
            ff.player = f.player;

            _context.Fighters.Update(ff);
            _context.SaveChanges();

            return Ok(); //NoContent();
        }

        // pozicijos keitimas
        [HttpPatch]
        public IActionResult PartialUpdate([FromBody] int position, int id)
        {
            var fighter = _context.Fighters.Find(id);
            if (fighter == null)
            {
                return NotFound();
            }
            else
            {
                fighter.position = position;

                _context.Fighters.Update(fighter);
                _context.SaveChanges();
            }
            return Ok();
            //return CreatedAtRoute("GetPlayer", new { id = player.Id }, player);
        }

        // DELETE api/values/5
        /*[HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var todo = _context.Fighters.Find(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Fighters.Remove(todo);
            _context.SaveChanges();
            return NoContent();
        }
    }
}




