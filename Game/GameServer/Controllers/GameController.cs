﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GameServer.Models;
using System.Net;
using System.Net.Http;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameServer.Controllers
{
    [Route("api/games")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly HavanaContext _context;
        public int Qty { get; set; } = 0;

        public GameController(HavanaContext context)
        {
            _context = context;
        }

        // GET api/player
        [HttpGet]
        public ActionResult<IEnumerable<Game>> GetAll()
        {
            return _context.Games.ToList();
        }

        // GET api/player/5/game
        [HttpPost()]
        public HttpResponseMessage CreateGame([FromBody] Player p)
        {
            if (p == null || p.game != null)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            //_context.Games.Entr(g => g.players).ToList();
            var game = _context.Games.Where(x => x.startedAt == null).FirstOrDefault();
            if (game != null)
            {
                game.addPlayer(p);
                if (game.players.Count >= 2)
                    game.startGame();
                p.game = game;
                _context.Games.Update(game);
            }
            else
            {
                var new_game = new Game();
                new_game.addPlayer(p);
                p.game = new_game;
                _context.Games.Update(new_game);
            }
            _context.Players.Update(p);
            _context.SaveChanges();

            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }
    }
    

}




