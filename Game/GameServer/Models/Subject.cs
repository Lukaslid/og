using System;
using System.Collections.Generic;

namespace GameServer.Models
{
	public class Subject
	{
        public int ID { get; set; }
        public virtual Player player { get; set; }
        public int player_id { get; set; }
        public Subject()
        {

        }
        List<IObserver> observers = new List<IObserver>();

        public void attach(IObserver player)
        {
            observers.Add(player);
            player.setSubject(this);
        }

        public void detach(IObserver player)
        {
            observers.Remove(player);
        }

        public void notifyObservers(string message)
        {
            foreach (var observer in observers)
            {
                observer.showMessage(message);
            }
        }

        public void DidDamageToTower(IObserver observer, int hitpoints)
        {
            string message = observer.getUsername() + "'s base is being demolished! Hitpoints: " + hitpoints;

            this.notifyObservers(message);
        }

    }
}
