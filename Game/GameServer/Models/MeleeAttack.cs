﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class MeleeAttack : IAttackType
    {
        public MeleeAttack()
        { }
        public void Attack()
        {
            System.Console.WriteLine("Melee attack");
        }
    }
}
