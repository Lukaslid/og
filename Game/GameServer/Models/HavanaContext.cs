﻿using Microsoft.EntityFrameworkCore;
using System;
using GameServer.DesignPatterns;

namespace GameServer.Models
{
    public class HavanaContext: DbContext
    {
        public HavanaContext()
        {

        }
        public HavanaContext(DbContextOptions<HavanaContext> options)
            : base(options)
        {
            
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Coordinates> Coordinates { get; set; }
        public DbSet<Tower> Towers { get; set; }
        public DbSet<Turret> Turrets { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Fighter> Fighters { get; set; }
        public DbSet<Army> Armies { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>()
               .HasOne(p => p.tower)
               .WithOne(t => t.player)
               .HasForeignKey<Tower>(t => t.player_id)
               .OnDelete(Microsoft.EntityFrameworkCore.DeleteBehavior.Cascade);

            modelBuilder.Entity<Player>()
               .HasOne(p => p.turret)
               .WithOne(t => t.player)
               .HasForeignKey<Turret>(t => t.player_id)
               .OnDelete(Microsoft.EntityFrameworkCore.DeleteBehavior.Cascade);

            modelBuilder.Entity<Player>()
               .HasOne(p => p.army)
               .WithOne(a => a.player)
               .HasForeignKey<Army>(a => a.player_id)
               .OnDelete(Microsoft.EntityFrameworkCore.DeleteBehavior.Cascade);

            //modelBuilder.Entity<Game>()
            //  .HasMany(g => g.players)
            //  .WithOne(p => p.game);

            modelBuilder.Entity<Player>()
                        .HasOne(p => p.game)
                        .WithMany(g => g.players);

            modelBuilder.Entity<Player>()
               .HasOne(p => p.subject)
               .WithOne(s => s.player)
               .HasForeignKey<Subject>(s => s.player_id)
               .OnDelete(Microsoft.EntityFrameworkCore.DeleteBehavior.Cascade);
        }

    }
}