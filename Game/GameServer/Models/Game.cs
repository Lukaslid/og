﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class Game
    {
        public virtual List<Player> players { get; set; }
        public int ID { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? startedAt { get; set; }


        public Game( DateTime createdAt, DateTime startedAt, List<Player> players)
        {
            this.createdAt = createdAt;
            this.startedAt = startedAt;
            this.players = players;
        }

        public Game()
        {
            this.players = new List<Player>();
            this.createdAt = DateTime.Now;
            this.startedAt = null;
        }

        public void addPlayer(Player p)
        {
            players.Add(p);
        }

        public Game startGame()
        {
            startedAt = DateTime.Now;
            return this;
        }
    }
}
