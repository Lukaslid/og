﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class Tower
    {
        public int ID { get; set; }
        public virtual Player player { get; set; }
        public int hitpoints { get; set; }
        public int player_id { get; set; }

        public Tower(Player pl)
        {
            this.player = pl;
            hitpoints = 10000;
        }

        public Tower()
        {

        }

        //public void TakeDamage(int damage)
        //{
        //    this.hitpoints = this.hitpoints - damage;
        //    player.subject.DidDamageToTower(player, this.hitpoints);
        //}
    }
}
