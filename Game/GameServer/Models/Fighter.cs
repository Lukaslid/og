﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class Fighter
    {
        public int ID { get; set; }
        public int hitpoints { get; set; }
        public int current_health { get; set; }
        public int damage { get; set; }
        public int movement_speed { get; set; }
        public int cooldown { get; set; }
        public int attack_speed { get; set; }
        public int attack_range { get; set; }
        public int price { get; set; }
        public int upgradePrice { get; set; }
        public string color { get; set; }
        public int level { get; set; }
        public double position { get; set; }
        public virtual Player player { get; set; }

        public Fighter(int hitpoints, int damage, int movement_speed, int cooldown,
                        int attack_speed, int attack_range, int price, int upgradePrice, string color, int level, Player p)
        {        
            this.hitpoints = hitpoints;
            this.current_health = hitpoints;
            this.damage = damage;
            this.movement_speed = movement_speed;
            this.cooldown = cooldown;
            this.attack_speed = attack_speed;
            this.attack_range = attack_range;
            this.price = price;
            this.upgradePrice = upgradePrice;
            this.color = color;
            this.level = level;
            this.position = 0;
            this.player = p;
        }

        public Fighter()
        {

        }
    }
}
