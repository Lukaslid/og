﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class SelfDestructAttack : IAttackType
    {
        public SelfDestructAttack()
        {

        }
        public void Attack()
        {
            System.Console.WriteLine("Self destructed");
        }
    }
}
