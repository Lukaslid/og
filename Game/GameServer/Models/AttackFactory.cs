﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class AttackFactory
    {
        //use GetAttack method to get object of type IAttackType
        public IAttackType GetAttack(string attackType)
        {
            if (attackType == null)
                return null;
            if (attackType.ToLower().Equals("melee"))
                return new MeleeAttack();
            else if (attackType.ToLower().Equals("ranged"))
                return new RangedAttack();
            else if (attackType.ToLower().Equals("selfdestruct"))
                return new SelfDestructAttack();

            return null;
        }
    }
}
