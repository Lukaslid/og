﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.Models
{
    public class Turret
    {
        public int ID { get; set; }
        public virtual Player player { get; set; }
        public int damage { get; set; }
        public int attack_speed { get; set; }
        public int level { get; set; }
        public int player_id { get; set; }

        public Turret()
        {

        }

        public Turret(Player pl)
        {
            player = pl;
            damage = 30;
            level = 1;
            attack_speed = 10;
        }
    }
}
