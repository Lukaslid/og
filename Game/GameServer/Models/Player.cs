﻿using System;
using GameServer.DesignPatterns;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameServer.Models
{
    public class Player : IObserver
    {
        public int ID { get; set; }
        public string username { get; set; }
        public int experience { get; set; }
        public int gold { get; set; }
        public virtual Tower tower { get; set; }
        public virtual Turret turret { get; set; }
        public virtual Subject subject { get; set; }
        public virtual Army army { get; set; }
        //public virtual int? gameID { get; set; }
        public virtual Game game { get; set; }

        public Player()
        {

        }

        public Player(string name, int exp, int cash)
        {
            username = name;
            experience = exp;
            gold = cash;

            tower = new Tower(this);
            turret = new Turret(this);
            subject = new Subject();
        }

        public void showMessage(string message )
        {
            Console.WriteLine(message);
        }

        public int getTowerHitpoints()
        {
            return tower.hitpoints;
        }

        public string getUsername()
        {
            return username;
        }

        public Subject getSubject()
        {
            return subject;
        }

        public void setSubject(Subject subject)
        {
            this.subject = subject;
        }
    }
}
