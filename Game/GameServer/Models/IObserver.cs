using System;
using System.Collections.Generic;

namespace GameServer.Models
{
	public interface IObserver
	{
		void showMessage( string message );
        int getTowerHitpoints();
        string getUsername();
        Subject getSubject();
        void setSubject(Subject subject);

    }	
}
