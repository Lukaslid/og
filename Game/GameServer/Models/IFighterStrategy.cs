/**
 * @(#) IFighterStrategy.cs
 */

namespace Class_diagram.Gameplay
{
	public interface IFighterStrategy
	{
		void executeAction(  );
		
	}
	
}
