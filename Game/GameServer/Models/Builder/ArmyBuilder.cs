﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.DesignPatterns
{
    public abstract class ArmyBuilder
    {
        public abstract void BuildArmy();
    }
}
