﻿using GameServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.DesignPatterns
{
    public class ConstructArmy : ArmyBuilder
    {
        private Army _army = new Army();

        public override void BuildArmy()
        {
            //Fighter f = new Fighter(100, 10, 10, 5, 10, 1, 100, "red", 1);  // todo add player
            //_army.AddFighter(f);
        }

        public void ReturnArmy()
        { }
    }
}
