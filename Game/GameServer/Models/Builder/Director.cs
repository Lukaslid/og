﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.DesignPatterns
{
    public class ArmyDirector
    {
        public void Construct(ArmyBuilder builder)
        {
            builder.BuildArmy();
        }
    }
}
