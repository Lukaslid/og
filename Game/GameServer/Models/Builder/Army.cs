﻿using GameServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameServer.DesignPatterns
{
    public class Army
    {
        public int ID { get; set; }
        private List<Fighter> _fighters = new List<Fighter>();
        public int player_id { get; set; }
        public virtual Player player { get; set; }

        public void AddFighter(Fighter fighter)
        {
            _fighters.Add(fighter);
        }
    }
}
