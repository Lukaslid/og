﻿using GameServer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameServer;

//namespace GameClientApp
//{
//    public partial class GameplayForm : Form
//    {
//        Player player { get; set; }

//        public GameplayForm(Player p)
//        {
//            InitializeComponent();
//            player = p;
//        }
//    }
//}

namespace GameClientApp
{
    public partial class GameplayForm : Form
    {
        Game game { get; set; }
        Player player { get; set; }
        List<Fighter> player1;
        List<Fighter> player2;
        FighterTypeFactory FTFactory;
        Dictionary<PictureBox, Fighter> fighterPictures;
        int counter;
        List<Fighter> updatedList;
        ArmyDirector director;
        ConstructArmy constructArmy;



        //TODO: change to singleton Timer class
        DateTime start;
        TimeSpan time;

        public GameplayForm(Player p)
        {
            InitializeComponent();
            player = new Player(); //Needs to bechanged to Game class

            SetParameters();
            InitializeVariables();
        }

        public void InitializeVariables()
        {
            FTFactory = new FighterTypeFactory();
            fighterPictures = new Dictionary<PictureBox, Fighter>();
            updatedList = new List<Fighter>();
            director = new ArmyDirector();
            constructArmy = new ConstructArmy();
            //Temporary lists
            player1 = new List<Fighter>();
            player2 = new List<Fighter>();
        }

        public void SetParameters()
        {
            DoubleBuffered = true;      //Enables smoother object movement
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;    //Disables window resizing
            this.MaximizeBox = false;   //Disables window maximization
            this.MinimizeBox = false;   //Disables window minimization
            timer1.Start();
            start = DateTime.Now;       //Start timer
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Timer
            time = DateTime.Now - start;
            labelTimer.Text = String.Format("{0}:{1}", time.Minutes.ToString().PadLeft(2, '0'), time.Seconds.ToString().PadLeft(2, '0'));

            //-----------------------

            //Get fighters list from DB
            foreach (KeyValuePair<PictureBox,Fighter> pair in fighterPictures)
            {
                bool matchFound = false;
                foreach (Fighter item in updatedList)
                {
                    if (pair.Value.ID == item.ID)
                    {
                        //Update fighter data
                        pair.Key.Location = new Point(Convert.ToInt32(item.position), 0);
                        matchFound = true;
                    }
                }
                if(!matchFound)
                {
                    string type = pair.Value.GetFighterType();
                    PictureBox pictureType = null;
                    switch(type)
                    {
                        case "warrior":
                            pictureType = warriorPicture;
                            break;
                        default:
                            break;
                    }
                    PictureBox pb = new PictureBox { Image = pictureType.Image, Width = pictureType.Width, Height = pictureType.Height, Location = new Point(0, 400) };
                    fighterPictures.Add(pb, pair.Value);
                    this.Controls.Add(pb);          //Instantiates object
                    //labelConsole.Text = fighterPictures.Count.ToString();
                }
            }

        }

        private void warriorButton_Click(object sender, EventArgs e)
        {
           
            director.Construct(constructArmy, "warrior", player);
            labelConsole.Text = player.army.Count.ToString();
            


            //PictureBox pictureType = warriorPicture;
            //Fighter f = new Fighter(); //TODO: Constructor would use FighterTypeFactory to get fighter parameters
            //f.ChangeID(69);
                                                 
            //PictureBox pb = new PictureBox { Image = pictureType.Image, Width = pictureType.Width, Height = pictureType.Height, Location = new Point(0, 400) };
            //fighterPictures.Add(pb, f);
            //this.Controls.Add(pb);          //Instantiates object
            labelConsole.Text = fighterPictures.Count.ToString();
        }

        private void rangerButton_Click(object sender, EventArgs e)
        {

        }

        //temp 
        private void tankButton_Click(object sender, EventArgs e)
        {
            Fighter x = new Fighter(10, 10, 10, 10, 10, 10, 10, 10, "blue", 10, new Player(), "test");
            x.ChangeID(69);
            x.ChangePosition(200);
            updatedList.Add(x);
            
        }

        private void GameplayForm_Load(object sender, EventArgs e)
        {            
        }

        private void labelTimer_Click(object sender, EventArgs e)
        {

        }
    }
}
