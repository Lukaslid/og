﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameServer.Controllers;
using GameServer.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GameClientApp
{
    public partial class LoginForm : Form
    {
        static HavanaContext context = new HavanaContext();
        public LoginForm()
        {
            InitializeComponent();
            button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.MouseDownBackColor = Color.Transparent;
            button1.FlatAppearance.MouseOverBackColor = Color.Transparent;
            button1.BackColor = Color.Transparent;

        }

        static HttpClient client = new HttpClient(new HttpClientHandler()
        {
            UseProxy = false,
            AllowAutoRedirect = true,
        });
        static string requestUri = "api/player/";
        static string mediaType = "application/json";

        static async Task<ICollection<Player>> GetAllPlayerAsync(string path)
        {
            ICollection<Player> players = null;
            HttpResponseMessage response = await client.GetAsync(path + "api/player");
            if (response.IsSuccessStatusCode)
            {
                players = await response.Content.ReadAsAsync<ICollection<Player>>();
            }
            return players;
        }

        static async Task RunAsync(string username, LoginForm loginForm)
        {
            client.BaseAddress = new Uri("https://localhost:5001/"); //api /player/");

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(mediaType));

            try
            {
                // Get all players
                ICollection<Player> playersList = await GetAllPlayerAsync(client.BaseAddress.PathAndQuery);
                foreach (var p in playersList)
                {
                    if (p.username == username)
                    {
                        GameplayForm form = new GameplayForm(p);
                        form.Show();
                        loginForm.Hide();
                        //Player player = new Player(p.username, 1, 1);
                        var url = await CreateGameAsync(p);
                        break;
                    }
                }

                MessageBox.Show("Invalid username");
            }
            catch(Exception e) { MessageBox.Show(e.Message); }

        }

        static async Task<Uri> CreateGameAsync(Player player)
        {
            //List<Player> plist = new List<Player>();
            //plist.Add(player);
            //Game game = new Game(DateTime.Now, DateTime.MinValue, plist);

            //string jsonString = JsonConvert.SerializeObject(game);    
            HttpResponseMessage response = await client.PostAsJsonAsync("api/games", player);
            response.EnsureSuccessStatusCode();

            //HttpContent httpContent = new StringContent(jsonString, System.Text.Encoding.UTF8, mediaType);
            //HttpRequestMessage request = new HttpRequestMessage
            //{
            //    Method = new HttpMethod("POST"),
            //    RequestUri = new Uri(client.BaseAddress + "api/games/"),
            //    Content = httpContent,
            //};

            //HttpResponseMessage response = await client.SendAsync(request);
            return response.Headers.Location;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            RunAsync(username.Text, this).GetAwaiter();
        }
    }
}
