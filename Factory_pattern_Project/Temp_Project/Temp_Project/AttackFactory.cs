namespace Temp_Project
{
    public class AttackFactory
    {
        //use GetAttack method to get object of type IAttackType
        public IAttackType GetAttack(string attackType)
        {
            if (attackType == null)
                return null;
            if (attackType.ToLower().Equals("melee"))
                return new MeleeAttack();
            else if (attackType.ToLower().Equals("ranged"))
                return new RangedAttack();
            else if (attackType.ToLower().Equals("selfdestruct"))
                return new SelfDestructAttack();

            return null;
        }
    }
}