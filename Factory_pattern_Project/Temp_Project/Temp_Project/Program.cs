﻿using System;

namespace Temp_Project
{
    class Program
    {
        static void Main(string[] args)
        {
            AttackFactory af = new AttackFactory();

            IAttackType type1 = af.GetAttack("melee");
            IAttackType type2 = af.GetAttack("ranged");
            IAttackType type3 = af.GetAttack("selfdestruct");

            type1.Attack();
            type2.Attack();
            type3.Attack();

            Console.WriteLine();
            Console.WriteLine("Tipas: " + type1.GetType().ToString());
        }
    }
}
