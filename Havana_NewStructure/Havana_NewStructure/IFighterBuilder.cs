﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public interface IFighterBuilder
    {
        void SetAttributes();
        void SetFighterType();
        //void SetAttackType();
        //void SetFighterStrategy();
        Fighter GetFighter();
    }
}
