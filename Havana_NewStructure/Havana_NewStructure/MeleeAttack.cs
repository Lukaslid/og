﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public class MeleeAttack : IAttackType
    {
        public MeleeAttack()
        { }

        public void Attack(Fighter friendlyFighter, Fighter enemyFighter)
        {
            //Melee action/animation()
            enemyFighter.TakeDamage(friendlyFighter.GetDamage());
        }
    }
}
