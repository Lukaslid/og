﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public class WarriorBuilder : IFighterBuilder
    {
        Fighter fighter = new Fighter();

        public void SetAttributes()
        {
            fighter.SetAttributes(
                100,    //Hitpoints
                10,     //Damage
                100,    //CurrentHitpoints
                10,     //MovementSpeed
                1,      //AttackSpeed
                1,      //AttackRange
                1,      //Level
                120     //Position (X coordinate)        
                );
        }

        public void SetFighterType()
        {
            fighter.SetFighterType("Warrior");
        }

        public Fighter GetFighter()
        { return fighter; }
    }
}
