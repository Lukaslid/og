﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public class RangedAttack : IAttackType
    {
        public RangedAttack()
        { }

        public void Attack(Fighter friendlyFighter, Fighter enemyFighter)
        {
            //Ranged action/animation()
            enemyFighter.TakeDamage(friendlyFighter.GetDamage());
        }
    }
}
