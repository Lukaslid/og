﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public class SelfDestructAttack : IAttackType
    {
        public SelfDestructAttack()
        { }

        public void Attack(Fighter friendlyFighter, Fighter enemyFighter)
        {
            //Self-destruct action/animation()
            enemyFighter.TakeDamage(friendlyFighter.GetDamage());
            friendlyFighter.TakeDamage(99999);
        }
    }
}
