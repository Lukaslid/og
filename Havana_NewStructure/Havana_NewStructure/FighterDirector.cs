﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    class FighterDirector
    {
        private IFighterBuilder fighterBuilder;

        public FighterDirector(IFighterBuilder fighterBuilder)
        { this.fighterBuilder = fighterBuilder; }

        public void CreateFighter()
        {
            fighterBuilder.SetAttributes();
            fighterBuilder.SetFighterType();
        }

        public Fighter GetFighter()
        { return fighterBuilder.GetFighter(); }
    }
}
