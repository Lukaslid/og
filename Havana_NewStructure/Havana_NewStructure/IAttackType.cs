﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public interface IAttackType
    {
        void Attack(Fighter friendlyFighter, Fighter enemyFighter);
    }
}
