﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Havana_NewStructure
{
    public class Fighter
    {
        public int ID { get; set; }                     //Set by Azure server

        int hitpoints { get; set; }
        int damage { get; set; }
        int currentHitpoints { get; set; }
        int movementSpeed { get; set; }
        int attackSpeed { get; set; }
        int attackRange { get; set; }
        int level { get; set; }
        int position { get; set; }
        

        string fighterType { get; set; }                //Builder pattern
        IFighterStrategy fighterStrategy { get; set; }  //Strategy pattern
        AttackFactory attackFactory { get; set; }       //Factory pattern

        public Fighter()
        { }

        //kodel ne paprastas konstruktorius,o set atrributes?
        public void SetAttributes(int hitpoints, int damage, int currentHitpoints, int movementSpeed, int attackSpeed, int range,int level, int position)
        {
            this.hitpoints = hitpoints;
            this.damage = damage;
            this.currentHitpoints = currentHitpoints;
            this.movementSpeed = movementSpeed;
            this.attackSpeed = attackSpeed;
            this.attackRange = range;
            this.level = level;
            this.position = position;
        }

        public void SetFighterType(string fighterType)
        {
            this.fighterType = fighterType;
        }

        public void GetAttack(string attackType)
        {
            var attack = attackFactory.GetAttack(attackType);
        }

        public int GetDamage()
        { return damage; }

        public override string ToString()
        {
            string x = String.Format("ID: {2} HP:{0} Damage: {1} | {3}", hitpoints, damage, ID, fighterType);
            return x;
        }

        public void ExecuteAction()
        {
            fighterStrategy.ExecuteAction();
        }

        public void SetID(int ID)
        { this.ID = ID; }

        public void TakeDamage(int damage)
        { hitpoints -= damage; }
        
        public void Attack(List<Fighter> enemyFighters)
        {
            if (enemyFighters[0].position <= this.position + this.attackRange)
            {
                attackFactory.GetAttack(fighterType).Attack(this, enemyFighters[0]);
            }                
        }
    }
}
